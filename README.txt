INTRODUCTION
------------

FlatEarth is a modern, clean, simple, and responsive theme for Drupal 8, with design inspired by the Flat Design conventions.

Project Page: https://www.drupal.org/sandbox/shivensinha4/2842413

REQUIREMENTS
------------

No special requirements. This theme has the base theme as Classy.

INSTALLATION
------------

Install as you would normally install a contributed Drupal theme. See:
https://www.drupal.org/node/1897420
for further information

CONFIGURATION
-------------

This theme does not require much additional configuration. You may add the desired blocks to the theme.

It is advised to NOT use this theme for the administration pages.